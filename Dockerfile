FROM ubuntu:xenial

MAINTAINER flanked 

ENV DEBIAN_FRONTEND noninteractive

ENV uid 1000
ENV gid 1000

# General Tools
RUN apt-get update && apt-get install -y wget vim-tiny
# Janus Dependencies
RUN apt-get install -y --no-install-recommends libmicrohttpd-dev libjansson-dev libnice-dev \
    libssl-dev libsrtp-dev libsofia-sip-ua-dev libglib2.0-dev \
    libopus-dev libogg-dev libcurl4-openssl-dev pkg-config gengetopt \
    libtool automake

WORKDIR /opt/janus
COPY . /opt/janus

RUN sh autogen.sh
RUN ./configure --prefix=/opt/janus && make && make install
RUN make configs

EXPOSE 8088

CMD ["/opt/janus/bin/janus"]
